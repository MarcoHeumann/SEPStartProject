package sepstartproject.views;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.util.Duration;
import sepstartproject.models.Rechteck;

/**
 * Das passende ViewModel zur eigentlichen FXML Datei. Dies ist die Schnittstelle zwischen der View und den Models.
 * Alle Anbindungen funktionieren via Databinding.
 * Dieses ViewModel wird automatisch erstellt sobald die verbundene FXML-Datei geladen wird.
 * 
 * @author Marco
 *
 */
public class RotatingViewModel
{
	@FXML
	private Rechteck	rotatingRect;
	@FXML
	private Button		toggleButton;
	@FXML
	private Label		infoLabel;
	/**
	 * sind Animationen pausiert?
	 */
	private boolean		isRunning	= false;
	/**
	 * gewünschtes Updateinterval in ms
	 */
	private int			updateCycle	= 16;
	/**
	 * Timer zur Animation.
	 */
	private Timeline	timeLine;

	/**
	 * Konstruktor wird automatisch aufgerufen sobald die FXML-Datei geladen wird.
	 * Eine Parameterübergabe ist also nicht direkt möglich, sollte je nach Programmierpattern aber auch nicht nötig sein.
	 */
	public RotatingViewModel()
	{
		// TODO Das sollte ein Logger sein!
		System.out.println("Rotating VM created. Sollte ein Logger machen!");
		// Methoden aufrufen die gestartet werden sollen wenn dieses ViewModel erstellt wird
		createStuff();
		setupStuff();
	}

	/**
	 * Alles was erstellt werden muss
	 */
	private void createStuff()
	{
		timeLine = new Timeline();
	}

	/**
	 * Alles was eingestellt werden muss
	 */
	private void setupStuff()
	{
		// Wie lange soll unser TickTimer laufen? Für immer.
		timeLine.setCycleCount(Timeline.INDEFINITE);
		// was soll der TickTimer machen wenn ein Cycle durch ist? Die updateStuff() Methode aufrufen
		timeLine.getKeyFrames().add(new KeyFrame(Duration.millis(updateCycle), ae -> updateStuff()));
	}

	/**
	 * Alles was "pro Tick" im {@link updateCycle} updated oder erledigt werden soll.
	 */
	private void updateStuff()
	{
		// Rotation anpassen
		rotatingRect.updateRotation();
		// Labeltext auch anpassen
		infoLabel.setText("Rotation ist: " + rotatingRect.rotateProperty().get() + " Grad");
	}

	/**
	 * ein Toggle das hin und her wechselt. Etwas eleganter als zwei Knöpfe zu nutzen.
	 * Können Sie als Ergänzung den Text des Knopfes entsprechend anpassen?
	 */
	@FXML
	private void toggleRotation()
	{
		if (isRunning)
		{
			isRunning = false;
			stopIt();
		}
		else
		{
			isRunning = true;
			startIt();
		}
	}

	private void startIt()
	{
		timeLine.play();
	}

	private void stopIt()
	{
		timeLine.stop();
	}
}
