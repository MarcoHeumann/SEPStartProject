package sepstartproject.models;

import javafx.scene.shape.Rectangle;

/**
 * Ein Rechteck, dass auf einer Rectangle-Node basiert.
 * Es soll als Beispiel fungieren wie man Grafische Elemente um eigene Parameter erweitern kann.
 * Hier sind dies eine Methode um die Roation zu ändern, sowie einen Multiplikator um diese Veränderung
 * schneller umzusetzen. So können unterschiedliche Rechtecke unterschiedliche Drehgeschwindigkeiten haben.
 */
public class Rechteck extends Rectangle
{
	/**
	 * Geschwindigkeit der Rotation dieses Rechtecks
	 */
	private double rotSpeed = 1;

	/**
	 * Checkt ob das RotationsProperty plus Multiplikator noch unter 360 (Grad) sind.
	 * Wenn ja wird weitergedreht, ansonsten wird die Rotation zurückgesetzt.
	 * 
	 * Zu beachten: Bei dieser Implementation können sehr Hohe Werte von {@link rotSpeed} zu einer Unsauberen Darstellung
	 * bei der Rotation nahe 360 Grad führen. Evtl. fällt Ihnen ja eine bessere Lösung ein.
	 */
	public void updateRotation()
	{
		if (this.rotateProperty().get() + (1 * rotSpeed) < 360)
		{
			this.rotateProperty().set(this.rotateProperty().get() + (1 * rotSpeed));
		}
		else
		{
			this.rotateProperty().set(0);
		}
		System.out.println("Rotation: " + this.rotateProperty().get() + " <-- sollte auch ein Logger machen!");
	}
}