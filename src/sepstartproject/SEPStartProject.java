package sepstartproject;

import java.io.IOException;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

/**
 * Anfangsprojekt - hier soll ein Ball animiert werden. Als Ausgangsbasis können
 * Sie dieses Programm verwenden, dass ein sich drehendes Rechteck zeichnet.
 */
public class SEPStartProject extends Application
{
	/**
	 * Einstiegspunkt für JavaFX
	 */
	@Override
	public void start(Stage primaryStage)
	{
		// Titel des Fensters setzen
		primaryStage.setTitle("Drehendes Rechteck");
		// Try, weil das laden einer Datei auch schiefgehen kann
		try
		{
			// fxml Datei laden
			FXMLLoader loader = new FXMLLoader();
			GridPane rootLayout = (GridPane) loader.load(getClass().getResourceAsStream("views/RotatingView.fxml"));
			// Scene erstellen
			Scene scene = new Scene(rootLayout);
			// Style-Datei laden
			scene.getStylesheets().add(getClass().getResource("/styles.css").toExternalForm());
			// Scene setzen und anzeigen
			primaryStage.setScene(scene);
			primaryStage.show();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	/**
	 * Normale Java Einstiegsmethode
	 *
	 * @param args Command line parameters - ignored.
	 */
	public static void main(String[] args)
	{
		launch(args);
	}
}
